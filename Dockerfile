FROM centos:8

RUN dnf install -y git python3-lxml itstool python3 which automake autoconf \
        diffutils gcc gettext gettext-devel docbook-dtds

RUN mkdir -p /usr/local/lib/python3.6/site-packages/

RUN git clone https://github.com/projectmallard/pintail.git && \
    cd pintail && python3 setup.py build && python3 setup.py install && \
    cd / && rm -rf pintail

RUN git clone https://github.com/projectmallard/mallard-ducktype.git && \
    cd mallard-ducktype && python3 setup.py build && python3 setup.py install && \
    cd / && rm -rf mallard-ducktype

RUN git clone https://github.com/projectmallard/pintail-itstool.git && \
    cd pintail-itstool && python3 setup.py build && python3 setup.py install && \
    cd / && rm -rf pintail-itstool

RUN git clone https://gitlab.gnome.org/Infrastructure/help.gnome.org /opt/help-web
