<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:str="http://exslt.org/strings"
                xmlns:mal="http://projectmallard.org/1.0/"
                xmlns="http://www.w3.org/1999/xhtml"
                extension-element-prefixes="exsl"
                exclude-result-prefixes="mal str"
                version="1.0">

<xsl:param name="html.sidebar.left">
  <xsl:if test="$pintail.site.dir != '/'">
    <xsl:value-of select="'contents'"/>
  </xsl:if>
</xsl:param>
<xsl:param name="html.sidebar.right">
  <xsl:if test="$pintail.site.dir != '/'">
    <xsl:value-of select="'gnome-editlink sections'"/>
  </xsl:if>
</xsl:param>

<xsl:template mode="html.body.attr.mode"
              match="mal:page[contains(concat(' ', @style, ' '), 'gnome-front-page')]">
  <xsl:if test="$pintail.site.dir = '/'">
    <xsl:attribute name="class">
      <xsl:text>gnome-front-page</xsl:text>
    </xsl:attribute>
  </xsl:if>
</xsl:template>

<xsl:template name="html.head.top.custom">
</xsl:template>

<xsl:template name="html.head.custom">
  <xsl:if test="starts-with($pintail.site.dir, '/unstable/')">
    <meta name="robots" content="noindex"/>
  </xsl:if>
</xsl:template>

<xsl:template name="html.js.content.custom">
</xsl:template>

<xsl:template name="html.top.custom">
  <xsl:variable name="rootlink">
    <xsl:if test="$mal.link.extension != ''">
      <xsl:text>index</xsl:text>
      <xsl:value-of select="$mal.link.extension"/>
    </xsl:if>
  </xsl:variable>
  <header class="gnome-header">
    <nav>
      <div class="pagewide">
        <a id="home-link" class="gnome-navbar-brand" title="Go to home page" href="{$mal.site.root}{$rootlink}"><img src="https://www.gnome.org/wp-content/uploads/2020/08/cropped-logo.png" alt="GNOME: The Free Software Desktop Project"/></a>
      </div>
    </nav>
  </header>
  <xsl:if test="$pintail.site.dir = '/'">
    <div class="gnome-banner">
      <img src="gnome-banner.svg"/>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="html.bottom.custom">
</xsl:template>

<xsl:template match="mal:links[@type = 'gnome:documents']">
  <div class="links-tiles gnome-help">
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/net.html">
        <span class="links-tile-banner"><img src="banner-network.svg"/></span>
        <span class="links-tile-text">Networking</span>
        <span class="links-tile-desc">Connect to wireless and wired networks. Stay safe
        with a VPN. Create a wireless hotspot.</span>
      </a>
    </div>
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/a11y.html">
        <span class="links-tile-banner"><img src="banner-a11y.svg"/></span>
        <span class="links-tile-text">Universal access</span>
        <span class="links-tile-desc">GNOME includes assistive technologies to help
        users with various impairments and special needs.</span>
      </a>
    </div>
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/tips.html">
        <span class="links-tile-banner"><img src="banner-tips.svg"/></span>
        <span class="links-tile-text">Tips &amp; tricks</span>
        <span class="links-tile-desc">From screencasts to special characters, learn
        how to make GNOME work even better for your life.</span>
      </a>
    </div>
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/files.html">
        <span class="links-tile-banner"><img src="banner-files.svg"/></span>
        <span class="links-tile-text">Files, folders &amp; search</span>
        <span class="links-tile-desc">Find and manage your files, whether on your
        computer, on the internet, or in backups.</span>
      </a>
    </div>
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/prefs.html">
        <span class="links-tile-banner"><img src="banner-settings.svg"/></span>
        <span class="links-tile-text">User &amp; system settings</span>
        <span class="links-tile-desc">From devices to privacy, make GNOME
        work for you.</span>
      </a>
    </div>
    <div class="links-tile">
      <a href="{$pintail.site.root}/gnome-help/index.html">
        <span class="links-tile-banner"><img src="banner-help.svg"/></span>
        <span class="links-tile-text">Get more help</span>
        <span class="links-tile-desc">Hooking up a printer, learning to use
        workspaces, or wrapping your head around color management.
        Our extensive help has you covered.</span>
      </a>
    </div>
  </div>

  <div class="links-tiles gnome-sysadmin" style="margin-top:60px;">
    <div class="links-tile"/>
    <div class="links-tile">
      <a href="{$pintail.site.root}/system-admin-guide/index.html">
        <span class="links-tile-banner"><img src="banner-sysadmin.svg"/></span>
        <span class="links-tile-text">System Administrators</span>
        <span class="links-tile-desc">Learn how to administer GNOME for multiple users
        and multiple machines in enterprise environments.</span>
      </a>
    </div>
    <div class="links-tile"/>
  </div>

  <xsl:variable name="_apps">
    <apps>
      <xsl:for-each select="document('__pintail__/APPS.xml')/apps/app">
        <xsl:variable name="app" select="."/>
        <xsl:variable name="siteid" select="concat('/', $app/@docid, '/index')"/>
        <xsl:for-each select="$mal.cache/*[@id = $siteid]">
          <xsl:variable name="node" select="."/>
          <app docid="{$app/@docid}" siteid="{$siteid}" group="{$app/@group}" icon="{$app/@icon}">
            <xsl:variable name="title">
              <xsl:call-template name="mal.link.content">
                <xsl:with-param name="node" select="$node"/>
                <xsl:with-param name="xref" select="$node/@id"/>
                <xsl:with-param name="role" select="'text'"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name="sorttitle">
              <xsl:value-of select="translate(normalize-space($title),
                                    'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                    'abcdefghijklmnopqrstuvwxyz')"/>
            </xsl:attribute>
            <title>
              <xsl:value-of select="normalize-space($title)"/>
            </title>
            <xsl:if test="$node/mal:info/mal:desc">
              <desc>
                <xsl:value-of select="normalize-space($node/mal:info/mal:desc[1])"/>
              </desc>
            </xsl:if>
          </app>
        </xsl:for-each>
      </xsl:for-each>
    </apps>
  </xsl:variable>
  <xsl:variable name="apps" select="exsl:node-set($_apps)/apps"/>

  <section class="gnome-apps gnome-apps-core">
    <h2>Core apps</h2>
    <div class="links-tiles">
      <xsl:for-each select="$apps/app[@group='core']">
        <xsl:sort select="@sorttitle"/>
        <xsl:call-template name="_tile">
          <xsl:with-param name="app" select="."/>
        </xsl:call-template>
      </xsl:for-each>
      <div class="links-tile"/>
      <div class="links-tile"/>
      <div class="links-tile"/>
    </div>
  </section>

  <section class="gnome-apps">
    <h2>Great games</h2>
    <div class="links-tiles">
      <xsl:for-each select="$apps/app[@group='games']">
        <xsl:sort select="@sorttitle"/>
        <xsl:call-template name="_tile">
          <xsl:with-param name="app" select="."/>
        </xsl:call-template>
      </xsl:for-each>
      <div class="links-tile"/>
      <div class="links-tile"/>
      <div class="links-tile"/>
    </div>
  </section>

  <section class="gnome-apps">
    <h2>And more...</h2>
    <div class="links-tiles">
      <xsl:for-each select="$apps/app[@group='more']">
        <xsl:sort select="@sorttitle"/>
        <xsl:call-template name="_tile">
          <xsl:with-param name="app" select="."/>
        </xsl:call-template>
      </xsl:for-each>
      <div class="links-tile"/>
      <div class="links-tile"/>
      <div class="links-tile"/>
    </div>
  </section>
</xsl:template>

<xsl:template name="_tile">
  <xsl:param name="app" select="."/>
  <xsl:param name="docid" select="$app/@docid"/>
  <xsl:param name="siteid" select="$app/@siteid"/>
  <xsl:param name="icon" select="$app/@icon"/>
  <xsl:param name="iconsize" select="192"/>
  <xsl:param name="node" select="$mal.cache/*[@id=$siteid]"/>
  <div class="links-tile">
    <a>
      <xsl:attribute name="href">
        <xsl:call-template name="mal.link.target">
          <xsl:with-param name="node" select="$node"/>
          <xsl:with-param name="xref" select="$node/@id"/>
        </xsl:call-template>
      </xsl:attribute>
      <span class="links-tile-img">
        <xsl:choose>
          <xsl:when test="$icon = '#default'">
            <img src="help-browser.png" style="width:{$iconsize}px;height:{$iconsize}px;opacity:25%;"/>
          </xsl:when>
          <xsl:otherwise>
            <img src="{$icon}" style="width:{$iconsize}px;height:{$iconsize}px;"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
      <span class="links-tile-text">
        <xsl:variable name="title">
          <xsl:call-template name="mal.link.content">
            <xsl:with-param name="node" select="$node"/>
            <xsl:with-param name="xref" select="$node/@id"/>
            <xsl:with-param name="role" select="'text'"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="normalize-space($title)"/>
      </span>
      <xsl:if test="$node/mal:info/mal:desc">
        <span class="links-tile-desc">
          <xsl:value-of select="normalize-space($node/mal:info/mal:desc[1])"/>
        </span>
      </xsl:if>
    </a>
  </div>
</xsl:template>

<xsl:template name="mal.link.content.custom">
  <xsl:param name="node" select="."/>
  <xsl:param name="action" select="$node/@action"/>
  <xsl:param name="xref" select="$node/@xref"/>
  <xsl:param name="href" select="$node/@href"/>
  <xsl:param name="role" select="''"/>
  <xsl:param name="info" select="/false"/>
  <xsl:if test="contains(concat(' ', $node/@style, ' '), ' hgo-versions ')">
    <xsl:variable name="dirs" select="str:split($xref, '/')"/>
    <xsl:value-of select="$dirs[count($dirs) - 1]"/>
  </xsl:if>
</xsl:template>

<xsl:param name="pintail.git.repository"/>
<xsl:param name="pintail.git.branch"/>
<xsl:param name="pintail.git.directory"/>
<xsl:template mode="html.sidebar.mode" match="token[.='gnome-editlink']">
  <xsl:if test="starts-with($pintail.git.repository, 'https://gitlab.gnome.org/GNOME/')">
    <xsl:variable name="href">
      <xsl:choose>
        <xsl:when test="substring($pintail.git.repository, string-length($pintail.git.repository) - 3) = '.git'">
          <xsl:value-of select="substring($pintail.git.repository, 1, string-length($pintail.git.repository) - 4)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$pintail.git.repository"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text>/blob/</xsl:text>
      <xsl:value-of select="$pintail.git.branch"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="$pintail.git.directory"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="$pintail.source.file"/>
    </xsl:variable>
    <div class="link-button">
      <a href="{$href}">Edit on GitLab</a>
    </div>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
