<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="text"/>

<xsl:template match="/apps">
  <xsl:for-each select="group/app">
    <xsl:variable name="id" select="@id"/>
    <xsl:variable name="project">
      <xsl:choose>
        <xsl:when test="@project">
          <xsl:value-of select="@project"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>GNOME</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="directory">
      <xsl:choose>
        <xsl:when test="@directory">
          <xsl:value-of select="@directory"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>help/C</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="stable">
      <xsl:choose>
        <xsl:when test="@stable">
          <xsl:value-of select="@stable"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>master</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:text>[/</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>/]&#x0A;</xsl:text>
    <xsl:text>git_repository = https://gitlab.gnome.org/</xsl:text>
    <xsl:value-of select="$project"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text>.git&#x0A;git_directory = </xsl:text>
    <xsl:value-of select="$directory"/>
    <xsl:text>&#x0A;git_branch = </xsl:text>
    <xsl:value-of select="$stable"/>
    <xsl:if test="@docbook">
      <xsl:text>&#x0A;docbook = </xsl:text>
      <xsl:value-of select="@docbook"/>
    </xsl:if>
    <xsl:text>&#x0A;&#x0A;</xsl:text>
    <xsl:if test="@unstable">
      <xsl:text>[/unstable/</xsl:text>
      <xsl:value-of select="$id"/>
      <xsl:text>/]&#x0A;</xsl:text>
      <xsl:text>git_repository = https://gitlab.gnome.org/</xsl:text>
      <xsl:value-of select="$project"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="$id"/>
      <xsl:text>.git&#x0A;git_directory = </xsl:text>
      <xsl:value-of select="$directory"/>
      <xsl:text>&#x0A;git_branch = </xsl:text>
      <xsl:value-of select="@unstable"/>
      <xsl:if test="@docbook">
        <xsl:text>&#x0A;docbook = </xsl:text>
        <xsl:value-of select="@docbook"/>
      </xsl:if>
      <xsl:text>&#x0A;&#x0A;</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
