#!/bin/python3

STABLE_BRANCHES = ('gnome-41', 'gnome-40')

import json
import os.path
import sys
import urllib.request
import yaml

try:
    icons = []
    apps = yaml.full_load(open('APPS.yaml'))
    appsxml = open('__pintail__/APPS.xml', 'w')
    appsxml.write('<apps>\n')
    for group in apps:
        for app in apps[group]:
            icon = '#default'
            if os.path.exists('_appicons/' + app + '.svg'):
                icon = app + '.svg'
            elif os.path.exists('_appicons/' + app + '.png'):
                icon = app + '.png'
            if icon != '#default':
                icons.append('_appicons/' + icon)
            appsxml.write('<app docid="{0}" group="{1}" icon="{2}"/>\n'
                          .format(app, group, icon))
            appdata = apps[group][app] or {}
            directory = appdata.get('directory', 'help/C')
            stable = appdata.get('stable', None)
            unstable = appdata.get('unstable', None)
            if 'repository' in appdata:
                repository = appdata['repository']
                if stable is None:
                    stable = 'main'
            else:
                if 'project' in appdata:
                    project = appdata['project']
                else:
                    project = 'GNOME'
                repository = 'https://gitlab.gnome.org/' + project + '/' + app + '.git'
                if stable is None or unstable is None:
                    try:
                        branches_url = f"https://gitlab.gnome.org/api/v4/projects/{project}%2F{app}/repository/branches"
                        bfile = urllib.request.urlopen(branches_url)
                        branches = json.load(bfile)
                        branches = [branch['name'] for branch in branches]
                    except urllib.error.HTTPError as err:
                        print(f"Unable to get list of branches for {project}/{app} at {branches_url}: {err}")
                        branches = ['master']
                    if unstable is None:
                        unstable = 'main' if ('main' in branches) else 'master'
                    if stable is None:
                        stable = unstable
                        for trybranch in STABLE_BRANCHES:
                            if trybranch in branches:
                                stable = trybranch
                                break
            if stable == unstable:
                unstable = None
            print('[/' + app + '/]')
            print('git_repository = ' + repository)
            print('git_directory = ' + directory)
            print('git_branch = ' + stable)
            print('')
            if unstable is not None:
                print('[/unstable/' + app + '/]')
                print('git_repository = ' + repository)
                print('git_directory = ' + directory)
                print('git_branch = ' + unstable)
                print('')
    print('[/]')
    print('extra_files = ' + ' '.join(icons))
    appsxml.write('</apps>\n')
    appsxml.close()
except KeyboardInterrupt:
    sys.exit(1)
